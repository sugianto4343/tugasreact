import './App.css';
import React from 'react';

function App() {
  return (
    <div className="App">
      <body>
        <header><h1 contenteditable>Header.com</h1></header>
        <div className="left-sidebar" contenteditable>Left Sidebar</div>
        <main contenteditable></main>
        <div className="right-sidebar" contenteditable>Right Sidebar</div>
        <footer contenteditable>Footer Content — Header.com 2020</footer>
      </body>
    </div>
  );
}

export default App;
